package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import client.Mensageiro;

public class Gestor extends UnicastRemoteObject implements Mensageiro {
	private static final long serialVersionUID = 1L;

	public Gestor() throws RemoteException {
		super();
	}

	@Override
	public String hello(String nome) throws RemoteException {
		return "Olá, " + nome + "!";
	}

}
